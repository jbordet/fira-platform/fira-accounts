package com.jbdev.fira.platform.accounts.core.model.entities;

import com.jbdev.fira.platform.accounts.client.dto.enums.Cryptocurrency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Getter
@Setter
@Document
@NoArgsConstructor
@AllArgsConstructor
public class CryptoAccount extends Account {

    private String publicAddress;
    private Cryptocurrency cryptocurrency;
    private BigDecimal balance = BigDecimal.ZERO;

}
