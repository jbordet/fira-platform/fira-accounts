package com.jbdev.fira.platform.accounts.core.model.entities;

import com.jbdev.fira.platform.accounts.client.dto.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankingAccount extends Account {

    private BigDecimal balance = BigDecimal.ZERO;
    private Currency currency;
    private String accountNumber;
}
