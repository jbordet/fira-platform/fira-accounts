package com.jbdev.fira.platform.accounts.core.service.impl;

import com.jbdev.exception_utils.exceptions.BusinessException;
import com.jbdev.exception_utils.exceptions.NotFoundException;
import com.jbdev.fira.platform.accounts.client.constants.FiraAccountsErrorConstants;
import com.jbdev.fira.platform.accounts.client.dto.enums.Cryptocurrency;
import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountTransferRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.CryptoAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.response.AccountsSearchResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.BankingAccountResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.CryptoAccountResponseDto;
import com.jbdev.fira.platform.accounts.core.mapper.AccountsMapper;
import com.jbdev.fira.platform.accounts.core.model.entities.BankingAccount;
import com.jbdev.fira.platform.accounts.core.model.entities.CryptoAccount;
import com.jbdev.fira.platform.accounts.core.model.repository.BankingAccountRespository;
import com.jbdev.fira.platform.accounts.core.model.repository.CryptoAccountRepository;
import com.jbdev.fira.platform.accounts.core.service.AccountsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class AccountsServiceImpl implements AccountsService {

    @Autowired
    private CryptoAccountRepository cryptoAccountRepository;

    @Autowired
    private BankingAccountRespository bankingAccountRespository;

    @Autowired
    private AccountsMapper mapper;

    private final String ALIAS_WORDS_CSV_FILE_PATH = "csvs/alias_words.csv";


    @Override
    public AccountsSearchResponseDto findAllAccountsForCustomer() {
        //TODO: get customer keycloak id from security context
        String hardcodedCustomerId = "123456";

        List<CryptoAccountResponseDto> cryptoAccountsResponseDtoList = cryptoAccountRepository
                .findAllByUserId(hardcodedCustomerId)
                .stream()
                .map(cryptoAccount -> mapper.cryptoAccountToCryptoAccountResponseDto(cryptoAccount)).toList();

        List<BankingAccountResponseDto> bankingAccountsResponseDtoList = bankingAccountRespository
                .findAllByUserId(hardcodedCustomerId)
                .stream()
                .map(bankingAccount -> mapper.bankingAccountToBankingAccountResponseDto(bankingAccount)).toList();

        AccountsSearchResponseDto searchResponse = new AccountsSearchResponseDto();
        searchResponse.setCryptoAccounts(cryptoAccountsResponseDtoList);
        searchResponse.setBankingAccounts(bankingAccountsResponseDtoList);
        return searchResponse;
    }

    @Override
    public CryptoAccountResponseDto createCryptoAccount(CryptoAccountRequestDto requestDto) {
        //TODO: get customer keycloak id from security context
        String hardcodedCustomerId = "123456";
        //TODO: validate user doesn't already have a crypto account for this crypto

        CryptoAccount newAccount = mapper.cryptoAccountRequestDtoToCryptoAccount(requestDto);
        newAccount.setAlias(generateAccountAlias());
        newAccount.setPublicAddress(generatePublicAddress(requestDto.getCryptocurrency()));
        CryptoAccount savedEntity = cryptoAccountRepository.save(newAccount);

        return mapper.cryptoAccountToCryptoAccountResponseDto(savedEntity);
    }

    @Override
    public BankingAccountResponseDto createBankingAccount(BankingAccountRequestDto requestDto) {
        //TODO: get customer keycloak id from security context
        String hardcodedCustomerId = "123456";
        //TODO: validate user doesn't already have a banking account for this currency

        BankingAccount newAccount = mapper.bankingAccountRequestDtoToBankingAccount(requestDto);
        newAccount.setAccountNumber(generateAccountNumber());
        newAccount.setAlias(generateAccountAlias());
        BankingAccount savedEntity = bankingAccountRespository.save(newAccount);

        return mapper.bankingAccountToBankingAccountResponseDto(savedEntity);
    }

    @Override
    public void transferFiatBalance(String bankingAccountId, BankingAccountTransferRequestDto dto) {
        //TODO: validate origin account belongs to requester
        //TODO: validate origin account has enough balance
        //TODO: validate destination account exists
        BankingAccount originAccount = bankingAccountRespository.findByAccountNumber(bankingAccountId)
                .orElseThrow(NotFoundException::new);

        if (originAccount.getBalance().compareTo(dto.getAmount()) < 0) {
            throw new BusinessException(FiraAccountsErrorConstants.ORIGIN_ACCOUNT_HAS_INSUFFICIENT_BALANCE);
        }

        BankingAccount destinationAccount;
        if (StringUtils.isNotBlank(dto.getDetinationAccountNumber())) {
            destinationAccount = bankingAccountRespository.findByAccountNumber(dto.getDetinationAccountNumber())
                    .orElseThrow(NotFoundException::new);
            transferAccountBalance(originAccount, destinationAccount, dto.getAmount());
        } else if (StringUtils.isNotBlank(dto.getDestinationAccountAlias())) {
            destinationAccount = bankingAccountRespository.findByAlias(dto.getDestinationAccountAlias())
                    .orElseThrow(NotFoundException::new);
            transferAccountBalance(originAccount, destinationAccount, dto.getAmount());
        } else {
            throw new BusinessException(FiraAccountsErrorConstants.ACCOUNT_NUMBER_AND_ALIAS_CANNOT_BE_NULL_OR_EMPTY);
        }

        //TODO: generate new user transaction history entry

        bankingAccountRespository.saveAll(Arrays.asList(originAccount,
                destinationAccount));
    }

    private void transferAccountBalance(BankingAccount originAccount, BankingAccount destinationAccount,
                                        BigDecimal amount) {
        originAccount.setBalance(originAccount.getBalance().subtract(amount));
        destinationAccount.setBalance(destinationAccount.getBalance().add(amount));
    }

    private String generatePublicAddress(Cryptocurrency cryptocurrency) {
        //TODO: Generate/get public address
        return "placeholder_address";
    }

    private static String generateAccountNumber() {
        //TODO: Validate that account number doesn't already exist
        String hardcodedBankBranchNumber = "116";
        String hardcodedAccountVerificationNumber = "9";
        Integer randomFiveDigitNumber = 10000 + (int) (Math.random() * 90000);
        return hardcodedBankBranchNumber + randomFiveDigitNumber.toString() + hardcodedAccountVerificationNumber;
    }

    private String generateAccountAlias() {
        try {
            Path path = Paths.get(new ClassPathResource(ALIAS_WORDS_CSV_FILE_PATH).getURI());
            List<String> words = Files.readAllLines(path);
            Collections.shuffle(words);
            List<String> randomWords = words.subList(0, 3);
            //TODO: validate that alias is not in use
            return randomWords.get(0) + "." + randomWords.get(1) + "." + randomWords.get(2);
        } catch (IOException e) {
            log.error("Error reading the CSV file: " + ALIAS_WORDS_CSV_FILE_PATH + ". Error message: " + e.getMessage());
        }
        throw new BusinessException(FiraAccountsErrorConstants.ALIAS_COULD_NOT_BE_GENERATED,
                "Alias could not be generated");
    }

}
