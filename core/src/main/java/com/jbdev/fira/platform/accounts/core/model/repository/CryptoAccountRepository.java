package com.jbdev.fira.platform.accounts.core.model.repository;

import com.jbdev.fira.platform.accounts.core.model.entities.CryptoAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface CryptoAccountRepository extends MongoRepository<CryptoAccount, String> {

    Optional<CryptoAccount> getById(String id);

    List<CryptoAccount> findAllByUserId(String userId);

}
