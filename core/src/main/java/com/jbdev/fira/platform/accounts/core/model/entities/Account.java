package com.jbdev.fira.platform.accounts.core.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public abstract class Account {

    private String id;
    private String userId;
    private String alias;

}
