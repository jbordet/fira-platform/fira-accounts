package com.jbdev.fira.platform.accounts.core.rest;

import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountTransferRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.CryptoAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.response.AccountsSearchResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.BankingAccountResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.CryptoAccountResponseDto;
import com.jbdev.fira.platform.accounts.core.service.AccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/")
public class AccountsController {

    @Autowired
    private AccountsService accountsService;

    @GetMapping("/accounts")
    @PreAuthorize("hasAuthority('test-role-can-be-deleted')")
    public ResponseEntity<AccountsSearchResponseDto> getClientAccounts() {
        AccountsSearchResponseDto searchResponse = accountsService.findAllAccountsForCustomer();
        return new ResponseEntity<>(searchResponse, HttpStatus.OK);
    }

    @PostMapping("/crypto-accounts")
    @PreAuthorize("hasAuthority('test-role-can-be-deleted')")
    public ResponseEntity<CryptoAccountResponseDto> createCryptoAccount(@RequestBody CryptoAccountRequestDto requestDto) {
        CryptoAccountResponseDto responseDto = accountsService.createCryptoAccount(requestDto);
        return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
    }

    @PostMapping("/banking-accounts")
    @PreAuthorize("hasAuthority('test-role-can-be-deleted')")
    public ResponseEntity<BankingAccountResponseDto> createBankingAccount(@RequestBody BankingAccountRequestDto requestDto) {
        BankingAccountResponseDto responseDto = accountsService.createBankingAccount(requestDto);
        return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
    }

    @PatchMapping("/banking-accounts/{bankingAccountId}/transfers")
    @PreAuthorize("hasAuthority('test-role-can-be-deleted')")
    public ResponseEntity<Void> transferFiatBalance(@PathVariable("bankingAccountId") String bankingAccountId,
                                                    @RequestBody BankingAccountTransferRequestDto dto) {
        accountsService.transferFiatBalance(bankingAccountId, dto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
