package com.jbdev.fira.platform.accounts.core.model.entities;

import com.jbdev.fira.platform.accounts.client.dto.enums.Cryptocurrency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CryptoTransaction extends Transaction {

    private Cryptocurrency cryptocurrency;
    private String destinationAddress;
    private String originAddress;

}
