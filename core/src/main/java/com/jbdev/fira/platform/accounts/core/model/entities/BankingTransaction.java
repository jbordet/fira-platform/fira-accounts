package com.jbdev.fira.platform.accounts.core.model.entities;

import com.jbdev.fira.platform.accounts.client.dto.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankingTransaction extends Transaction {

    private Currency currency;
    private String detinationAccount;
    private String originAccount;

}
