package com.jbdev.fira.platform.accounts.core.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class Transaction {

    private BigDecimal amount;
    private LocalDateTime executedOn;

}
