package com.jbdev.fira.platform.accounts.core.mapper;

import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.CryptoAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.response.BankingAccountResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.CryptoAccountResponseDto;
import com.jbdev.fira.platform.accounts.core.model.entities.BankingAccount;
import com.jbdev.fira.platform.accounts.core.model.entities.CryptoAccount;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountsMapper {

    CryptoAccountResponseDto cryptoAccountToCryptoAccountResponseDto(CryptoAccount cryptoAccount);

    BankingAccountResponseDto bankingAccountToBankingAccountResponseDto(BankingAccount bankingAccount);

    CryptoAccount cryptoAccountRequestDtoToCryptoAccount(CryptoAccountRequestDto requestDto);

    BankingAccount bankingAccountRequestDtoToBankingAccount(BankingAccountRequestDto requestDto);

}
