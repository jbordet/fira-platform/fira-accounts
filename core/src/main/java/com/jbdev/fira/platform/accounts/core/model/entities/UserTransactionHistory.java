package com.jbdev.fira.platform.accounts.core.model.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document
public class UserTransactionHistory {

    private String id;
    private String userId;
    private List<CryptoTransaction> cryptoTransactionList;
    private List<BankingTransaction> bankingTransactionList;

}
