package com.jbdev.fira.platform.accounts.core.model.repository;

import com.jbdev.fira.platform.accounts.core.model.entities.BankingAccount;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface BankingAccountRespository extends MongoRepository<BankingAccount, String> {

    Optional<BankingAccount> getById(String id);

    List<BankingAccount> findAllByUserId(String userId);

    Optional<BankingAccount> findByAccountNumber(String accountNumber);

    Optional<BankingAccount> findByAlias(String alias);
}
