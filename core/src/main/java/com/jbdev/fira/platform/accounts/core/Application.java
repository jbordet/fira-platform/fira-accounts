package com.jbdev.fira.platform.accounts.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.jbdev.library.keycloakintegration",
        "com.jbdev.httputils.*",
        "com.jbdev.fira.platform.accounts.*",
        "com.jbdev.exception_utils.*"
})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
