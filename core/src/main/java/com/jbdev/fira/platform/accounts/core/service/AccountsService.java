package com.jbdev.fira.platform.accounts.core.service;

import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.BankingAccountTransferRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.request.CryptoAccountRequestDto;
import com.jbdev.fira.platform.accounts.client.dto.response.AccountsSearchResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.BankingAccountResponseDto;
import com.jbdev.fira.platform.accounts.client.dto.response.CryptoAccountResponseDto;

public interface AccountsService {

    AccountsSearchResponseDto findAllAccountsForCustomer();

    CryptoAccountResponseDto createCryptoAccount(CryptoAccountRequestDto requestDto);

    BankingAccountResponseDto createBankingAccount(BankingAccountRequestDto requestDto);

    void transferFiatBalance(String bankingAccountId, BankingAccountTransferRequestDto dto);
}
