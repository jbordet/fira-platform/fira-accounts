package com.jbdev.fira.platform.accounts.client.dto.response;

import com.jbdev.fira.platform.accounts.client.dto.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankingAccountResponseDto {

    private String accountNumber;
    private String alias;
    private BigDecimal balance;
    private Currency currency;

}
