package com.jbdev.fira.platform.accounts.client.dto.request;

import com.jbdev.fira.platform.accounts.client.dto.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankingAccountTransferRequestDto {

    private BigDecimal amount;
    private Currency currency;
    private String detinationAccountNumber;
    private String destinationAccountAlias;

}
