package com.jbdev.fira.platform.accounts.client.dto.request;

import com.jbdev.fira.platform.accounts.client.dto.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankingAccountRequestDto {

    private Currency currency;

}
