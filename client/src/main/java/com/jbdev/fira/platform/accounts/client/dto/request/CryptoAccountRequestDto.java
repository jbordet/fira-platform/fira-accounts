package com.jbdev.fira.platform.accounts.client.dto.request;

import com.jbdev.fira.platform.accounts.client.dto.enums.Cryptocurrency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CryptoAccountRequestDto {

    private Cryptocurrency cryptocurrency;

}
