package com.jbdev.fira.platform.accounts.client.dto.response;

import com.jbdev.fira.platform.accounts.client.dto.enums.Cryptocurrency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CryptoAccountResponseDto {

    private String publicAddress;
    private String alias;
    private Cryptocurrency cryptocurrency;
    private BigDecimal balance;

}
