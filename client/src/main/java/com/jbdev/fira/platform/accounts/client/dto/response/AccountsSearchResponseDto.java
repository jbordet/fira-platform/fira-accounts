package com.jbdev.fira.platform.accounts.client.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountsSearchResponseDto {

    // TODO: Add pagination logic/classes
    List<BankingAccountResponseDto> bankingAccounts;
    List<CryptoAccountResponseDto> cryptoAccounts;

}
