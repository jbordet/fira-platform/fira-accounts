package com.jbdev.fira.platform.accounts.client.dto.enums;

public enum Currency {

    UNITED_STATES_DOLLAR,
    ARGENTINIAN_PESO

}
