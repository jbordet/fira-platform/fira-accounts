package com.jbdev.fira.platform.accounts.client.constants;

public class FiraAccountsErrorConstants {

    public final static String ALIAS_COULD_NOT_BE_GENERATED = "ALIAS_COULD_NOT_BE_GENERATED";

    public static final String ORIGIN_ACCOUNT_HAS_INSUFFICIENT_BALANCE = "ORIGIN_ACCOUNT_HAS_INSUFFICIENT_BALANCE";
    public static final String ACCOUNT_NUMBER_AND_ALIAS_CANNOT_BE_NULL_OR_EMPTY = "ACCOUNT_NUMBER_AND_ALIAS_CANNOT_BE_NULL_OR_EMPTY";
}
