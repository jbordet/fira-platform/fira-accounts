package com.jbdev.fira.platform.accounts.client.dto.enums;

public enum Cryptocurrency {

    BTC,
    ETH,

}
